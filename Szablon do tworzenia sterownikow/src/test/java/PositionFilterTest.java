package Tests;

import com.mateusz.nawigacja.serwer.Interfaces.PositionFilterInterface;
import com.mateusz.nawigacja.serwer.Interfaces.VehicleInterface;

import com.mateusz.nawigacja.serwer.Navigation.VehiclePosition;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Objects;

public class PositionFilterTest {

    private static PositionFilterInterface filter;

    private static final VehiclePosition POSITION = null;
    private static final VehiclePosition EXPECTED_POSITION = null;

    @BeforeAll
    public static void initTest() {
        boolean readingError = false;
        File directory = new File("drivers/");
        for (File jar : Objects.requireNonNull(directory.listFiles()))
            try {
                ClassLoader loader = URLClassLoader.newInstance(
                        new URL[]{jar.toURI().toURL()}
                );

                Class controllerClass = Class.forName("PositionFilter", true, loader);
                filter = (PositionFilterInterface) controllerClass.newInstance();

            } catch (ClassNotFoundException e) {
                readingError = true;
                e.printStackTrace();
            } catch (MalformedURLException | IllegalAccessException | InstantiationException e) {
                e.printStackTrace();
            }
        if (readingError) {
            Assertions.fail("Nie udało się pomyślnie wczytać wszystkich sterowników pojazdów z katalogu drivers");
        }
    }

    @Test
    public void testFilter() {
        VehiclePosition filteredPosition = filter.filter(POSITION);
        Assertions.assertEquals(EXPECTED_POSITION, filteredPosition);
    }
}
