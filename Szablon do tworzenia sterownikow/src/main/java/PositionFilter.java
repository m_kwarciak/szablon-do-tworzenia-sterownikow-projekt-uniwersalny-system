import com.mateusz.nawigacja.serwer.Interfaces.PositionFilterInterface;
import com.mateusz.nawigacja.serwer.Navigation.VehiclePosition;

public class PositionFilter implements PositionFilterInterface{

    @Override
    public VehiclePosition filter(VehiclePosition position) {
        return position;
    }
}